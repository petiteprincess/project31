#include "pch.h"

#include <fstream>
#include <iostream>
#include <string>

#include "Parser.h"

// проверяем существование файла
bool is_file_exist(const std::string&);

int main()
{
	// проверяем существование файла
	if (!is_file_exist("model.dxf")) {
		std::cerr << "File example.dxf couldn't be opened.\n";
		return 1;
	}
	Parser parser;

	Shapes shapes = parser.parse("model.dxf");
	for (const Shape* shape : shapes.get_data())
	{
		shape->print();
	}
}

bool is_file_exist(const std::string& filename)
{
	std::ifstream ifs(filename);
	return ifs.is_open();
}
