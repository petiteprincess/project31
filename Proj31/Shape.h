﻿#pragma once

#include <string>
#include <vector>

class Shape
{
public:
	virtual void print() const = 0;
protected:
	// В спецификации указано, что это нечто идет после цифры 5
	std::string handle;
	// Название слоя
	std::string layer_name;
};

class Circle : public Shape
{
public:
	Circle(std::vector<std::string>::const_iterator &, const std::vector<std::string>::const_iterator&);

	float center_x;
	float center_y;
	float center_z;
	float radius;

	void print() const override;
};

class Ellipse : public Shape
{
public:
	Ellipse(std::vector<std::string>::const_iterator &, const std::vector<std::string>::const_iterator&);

	float center_x;
	float center_y;
	float center_z;
	float axis_endpoint_x;
	float axis_endpoint_y;
	float axis_endpoint_z;
	float extrusion_x;
	float extrusion_y;
	float extrusion_z;
	float minor2major_ratio;
	float start_param;
	float end_param;

	void print() const override;
};

class Point : public Shape
{
public:
	Point(std::vector<std::string>::const_iterator &, const std::vector<std::string>::const_iterator&);

	float location_x;
	float location_y;
	float location_z;

	void print() const override;
};

enum class PolylineFlag
{
	Default = 0,
	ClosedPolyline = 1,
	CurveFit = 2,
	SplineFit = 4,
	Polyline3d = 8,
	Mesh3d = 16,
	ClosedPolygonMesh = 32,
	PolyfaceMesh = 64,
	LinetypePattern = 128
};

class Polyline : public Shape
{
public:
	Polyline(std::vector<std::string>::const_iterator &, const std::vector<std::string>::const_iterator&);

	float elevation;
	PolylineFlag flag = PolylineFlag::Default;

	void print() const override;
};

class Vertex : public Shape
{
public:
	Vertex(std::vector<std::string>::const_iterator &, const std::vector<std::string>::const_iterator&);

	float location_x;
	float location_y;
	float location_z;

	void print() const override;
};
