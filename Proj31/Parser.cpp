#include "pch.h"
#include "Parser.h"
#include "Shape.h"

#include <array>
#include <cctype>
#include <fstream>
#include <iostream>
#include <map>
#include <streambuf>
#include <string>


Parser::Parser()
{
}

Shapes Parser::parse(const std::string &filename)
{
	std::string contents = read_file(filename);
	std::vector<std::string> lines = get_lines(contents);
	std::vector<Shape*> shapes = get_shapes(lines);
	return Shapes(shapes);
}

Parser::~Parser()
{
}

std::string Parser::read_file(const std::string &filename)
{
	std::ifstream ifs(filename);
	std::string str((std::istreambuf_iterator<char>(ifs)),
					 std::istreambuf_iterator<char>());
	return str;
}

std::vector<Shape*> Parser::get_shapes(const std::vector<std::string> &lines)
{
	std::vector<Shape*> result;
	auto current_pos = lines.begin();
	while (current_pos != lines.end())
	{
		if (*current_pos == "CIRCLE")
		{
			current_pos++;
			result.push_back(new Circle(current_pos, lines.end()));
		}
		else if (*current_pos == "ELLIPSE")
		{
			current_pos++;
			result.push_back(new Ellipse(current_pos, lines.end()));
		}
		else if (*current_pos == "POINT")
		{
			current_pos++;
			result.push_back(new Point(current_pos, lines.end()));
		}
		else if (*current_pos == "POLYLINE")
		{
			current_pos++;
			result.push_back(new Polyline(current_pos, lines.end()));
		}
		else if (*current_pos == "VERTEX")
		{
			current_pos++;
			result.push_back(new Vertex(current_pos, lines.end()));
		}
		else current_pos++;
	}
	return result;
}
	
std::vector<std::string> Parser::get_lines(const std::string &contents) 
{
	std::vector<std::string> lines;

	std::string buffer;
	for (auto it = contents.begin(); it != contents.end(); it++)
	{
		if (*it == ' ')
			continue;
		if (*it == '\n')
		{
			lines.push_back(buffer);
			buffer.clear();
			continue;
		}
		buffer.push_back(*it);
	}
	return lines;
}

bool Parser::is_integer(const std::string &str)
{
	return std::all_of(str.begin(), str.end(), isdigit) &&
		((str[0] == '0' && str.length() == 0) || str[0] != '0');;
}

bool Parser::is_float(const std::string &str)
{
	int dot_counter = 0;
	return std::all_of(str.begin(), str.end(), [&dot_counter](char ch) {
		bool isDot = ch == '.';
		if (isDot) dot_counter++;
		return std::isdigit(ch) || (isDot && dot_counter == 1);
	});;
}


