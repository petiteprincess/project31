#include "pch.h"
#include "Shape.h"

#include <iostream>
#include <sstream>

Circle::Circle(std::vector<std::string>::const_iterator &current, const std::vector<std::string>::const_iterator &end)
{
	while (current != end)
	{
		if (*current == "5")
		{
			current++;
			handle = *current;
		}
		else if (*current == "8")
		{
			current++;
			layer_name = *current;
		}
		else if (*current == "10")
		{
			current++;
			center_x = std::stof(*current);
		}
		else if (*current == "20")
		{
			current++;
			center_y = std::stof(*current);
		}
		else if (*current == "30")
		{
			current++;
			center_z = std::stof(*current);
		}
		else if (*current == "40")
		{
			current++;
			radius = std::stof(*current);
		}
		else if (*current == "0")
		{
			current++;
			break;
		}
		current++;
	}
}

void Circle::print() const
{
	std::cout << "CIRCLE {\n"
		<< "center_x = " << center_x << "\n"
		<< "center_y = " << center_y << "\n"
		<< "center_z = " << center_z << "\n"
		<< "}" << std::endl;
}

Ellipse::Ellipse(std::vector<std::string>::const_iterator &current, const std::vector<std::string>::const_iterator &end)
{
	while (current != end)
	{
		if (*current == "5")
		{
			current++;
			handle = *current;
		}
		else if (*current == "8")
		{
			current++;
			layer_name = *current;
		}
		else if (*current == "10")
		{
			current++;
			center_x = std::stof(*current);
		}
		else if (*current == "20")
		{
			current++;
			center_y = std::stof(*current);
		}
		else if (*current == "30")
		{
			current++;
			center_z = std::stof(*current);
		}
		else if (*current == "40")
		{
			current++;
			minor2major_ratio = std::stof(*current);
		}
		else if (*current == "41")
		{
			current++;
			start_param = std::stof(*current);
		}
		else if (*current == "42")
		{
			current++;
			end_param = std::stof(*current);
		}
		else if (*current == "11")
		{
			current++;
			axis_endpoint_x = std::stof(*current);
		}
		else if (*current == "21")
		{
			current++;
			axis_endpoint_y = std::stof(*current);
		}
		else if (*current == "31")
		{
			current++;
			axis_endpoint_z = std::stof(*current);
		}
		else if (*current == "210")
		{
			current++;
			extrusion_x = std::stof(*current);
		}
		else if (*current == "220")
		{
			current++;
			extrusion_y = std::stof(*current);
		}
		else if (*current == "230")
		{
			current++;
			extrusion_z = std::stof(*current);
		}
		else if (*current == "0")
		{
			current++;
			break;
		}
		current++;
	}
}

void Ellipse::print() const
{
	std::cout << "ELLIPSE {\n"
		<< "center_x = " << center_x << "\n"
		<< "center_y = " << center_y << "\n"
		<< "center_z = " << center_z << "\n"
		<< "axis_endpoint_x = " << axis_endpoint_x << "\n"
		<< "axis_endpoint_y = " << axis_endpoint_y << "\n"
		<< "axis_endpoint_z = " << axis_endpoint_z << "\n"
		<< "extrusion_x = " << extrusion_x << "\n"
		<< "extrusion_y = " << extrusion_y << "\n"
		<< "extrusion_z = " << extrusion_z << "\n"
		<< "minor_to_major_ration = " << minor2major_ratio << "\n"
		<< "start_param = " << start_param << "\n"
		<< "end_param = " << end_param << "\n"
		<< "}" << std::endl;
}

Point::Point(std::vector<std::string>::const_iterator &current, const std::vector<std::string>::const_iterator &end)
{
	while (current != end)
	{
		if (*current == "5")
		{
			current++;
			handle = *current;
		}
		else if (*current == "8")
		{
			current++;
			layer_name = *current;
		}
		else if (*current == "10")
		{
			current++;
			location_x = std::stof(*current);
		}
		else if (*current == "20")
		{
			current++;
			location_y = std::stof(*current);
		}
		else if (*current == "30")
		{
			current++;
			location_z = std::stof(*current);
		}
		else if (*current == "0")
		{
			current++;
			break;
		}
		current++;
	}
}

void Point::print() const
{
	std::cout << "POINT {\n"
		<< "location_x = " << location_x << "\n"
		<< "location_y = " << location_y << "\n"
		<< "location_z = " << location_z << "\n"
		<< "}" << std::endl;
}

Polyline::Polyline(std::vector<std::string>::const_iterator &current, const std::vector<std::string>::const_iterator &end)
{
	while (current != end)
	{
		if (*current == "5")
		{
			current++;
			handle = *current;
		}
		else if (*current == "8")
		{
			current++;
			layer_name = *current;
		}
		else if (*current == "30")
		{
			current++;
			elevation = std::stof(*current);
		}
		else if (*current == "70")
		{
			current++;
			flag = static_cast<PolylineFlag>(std::stoi(*current));
		}
		else if (*current == "0")
		{
			current++;
			break;
		}
		current++;
	}
}

void Polyline::print() const
{
	std::cout << "POLYLINE {\n"
		<< "elevation = " << elevation << "\n"
		<< "flag = " << static_cast<int>(flag) << "\n"
		<< "}" << std::endl;
}

Vertex::Vertex(std::vector<std::string>::const_iterator &current, const std::vector<std::string>::const_iterator &end)
{
	while (current != end)
	{
		if (*current == "5")
		{
			current++;
			handle = *current;
		}
		else if (*current == "8")
		{
			current++;
			layer_name = *current;
		}
		else if (*current == "10")
		{
			current++;
			location_x = std::stof(*current);
		}
		else if (*current == "20")
		{
			current++;
			location_y = std::stof(*current);
		}
		else if (*current == "30")
		{
			current++;
			location_z = std::stof(*current);
		}
		else if (*current == "0")
		{
			current++;
			break;
		}
		current++;
	}
}

void Vertex::print() const
{
	std::cout << "VERTEX {\n"
		<< "location_x = " << location_x << "\n"
		<< "location_y = " << location_y << "\n"
		<< "location_z = " << location_z << "\n"
		<< "}" << std::endl;
}
