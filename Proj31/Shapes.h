#pragma once

#include <vector>

#include "Shape.h"

class Shapes
{
public:
	Shapes(const std::vector<Shape*>& shapes)
		: data(shapes) {}

	std::vector<Shape*>& get_data()
	{
		return data;
	}

	~Shapes()
	{
		auto it = data.begin();
		for (; it != data.end(); it++)
			delete *it;
		data.clear();
	}

private:
	std::vector<Shape*> data;
};