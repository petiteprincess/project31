#pragma once

#include <string>
#include <vector>

#include "Shape.h"
#include "Shapes.h"

class Parser
{
public:
	Parser();

	Shapes parse(const std::string&);

	~Parser();

private:
	// чтение файла
	std::string read_file(const std::string&);

	// получение фигур
    std::vector<Shape*> get_shapes(const std::vector<std::string>&);
	
	// получение строк из файла
	std::vector<std::string> get_lines(const std::string&);

	// проверка на целое число
	bool is_integer(const std::string&);

	// проверка на дробное число
	bool is_float(const std::string&);
};

